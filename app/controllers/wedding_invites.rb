class WeddingInvitesController < ApplicationController
  
  def create
    emails = WeddingGuest.where(wedding_id: params["wedding_id"]).email
    emails.each do |email|
      invite = Invite.new(wedding_id: wedding_id, email: email)
      if invite.save
        WeddingInviteMailer.invite(invitation_response_wedding_wedding_guest_path, ceva_path(invite_token: invite.token)).deliver
      end
    end
  end
end
